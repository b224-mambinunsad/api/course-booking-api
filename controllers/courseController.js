const Course = require("../models/Course.js");


// Controller Functions

// Creating a new course
module.exports.addCourse = (reqBody, userData) => {
	return Course.findById(userData.id).then(result => {
		if(userData.isAdmin == false){
			return false
		}else {
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newCourse.save().then((course, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		}
	})
};



// Function for retrieving all courses

module.exports.getAllCourses = (data) => {
	if(data.isAdmin){
		return Course.find({}).then(result => {
			return result
		})
	}else {
		return false
	}
};

// Function for retrieving all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then((result, error) => {
		if(error) {
			return false
		}else {
			return result
		}
	})
};


// Function for retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})


	// try {
	// 	return Course.findById(reqParams.courseId).then(result=> {
	// 		return result
	// 	})
	// } catch(error) {
	// 	return false
	// }
};


// Function for updating a specific course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
		if(error){
			return false
		}else {
			return true
		}
	})
}


// // Function for archiving a specific course
// module.exports.archiveCourse = (reqParams, reqBody) => {
// 	let archivedCourse = {
// 		isActive: reqBody.isActive
// 	}

// 	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((result,error) => {
// 		if (error){
// 			return false
// 		}else {
// 			return true
// 		}
// 	})
// };


// Function for archiving a specific course
module.exports.archiveCourse =  async (reqParams, reqBody) => {
	try{
		let archivedCourse = {
			isActive: reqBody.isActive
		}

		await Course.findByIdAndUpdate(reqParams.courseId, archivedCourse)
		return true
	} catch(error){
		return "error from try catch"
	} 
};


