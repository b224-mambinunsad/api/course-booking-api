const User = require("../models/User.js");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");




// Controller Functions


// Function for checking email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		}else{
			return false
		}
	})
};


// Function for user registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false
		// User registration successful
		}else {
			return true
		}
	})
};


// User Log in

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else {
				return false
			}
		}
	})
};


// // Function for getting User Details
// module.exports.getProfile = (reqBody) => {
// 	return User.findOne({_id: reqBody.id}).then(result => {
// 		if(result == null){
// 			return false
// 		}else {
// 			result.password = "***"
// 			return result
// 		}
// 	})
// };

// s38 Activity Solution
// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		if(result == null) {
			return false
		}else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


// Function for enrolling authenticated user
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId})
		return user.save().then((user, error) => {
			if(error){
				return false
			}else {
				return true
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId})
		return course.save().then((course, error) => {
			if(error){
				return false
			}else {
				return true
			}
		})
	});

	if(isUserUpdated && isCourseUpdated){
		// Enrollment succesful
		return true
	}else {
		return false
	};

};
